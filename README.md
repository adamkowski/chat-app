# Chat App

A small, real-time chat application.

## About

Using WebSocket for dynamic message exchange. Built with SpringBoot, managed with Gradle, and deployed with Kubernetes.

## How to run

First, for now, you need to create the images locally:

```
docker build -t chat-server:latest ./chat-server
docker build -t chat-client:latest ./chat-client
```

Then you can deploy it with Kubernetes:

```
kubectl apply -f deployment.yaml
```

Finally, the application will be available in `http://localhost:8081`, after a few seconds.
