package com.gitlab.adamkowski.chatservet.controller;

import com.gitlab.adamkowski.chatservet.model.Input;
import com.gitlab.adamkowski.chatservet.model.Message;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

@Controller
public class MessageController {

    @MessageMapping("/send/{dest}")
    @SendTo("/queue/{dest}")
    public Message send(Input input, @DestinationVariable String dest) {
        return new Message(input, LocalDateTime.now());
    }
}
