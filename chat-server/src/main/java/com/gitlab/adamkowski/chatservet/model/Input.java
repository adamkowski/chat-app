package com.gitlab.adamkowski.chatservet.model;

public record Input(String author, String content) {
}
