package com.gitlab.adamkowski.chatservet.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public record Message(String author, String content, String dateTime) {
    public Message(Input input, LocalDateTime dateTime) {
        this(input.author(), input.content(), dateTime.format(DateTimeFormatter.ISO_DATE_TIME));
    }
}
