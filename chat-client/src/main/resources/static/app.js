let stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
}

function connect() {
    const socket = new SockJS('http://localhost:8080/chat-app');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log(frame);

        const nickname = $("#nickname").val();
        stompClient.subscribe(`/queue/` + nickname, function (message) {
            console.log(message)
            showMessage(JSON.parse(message.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
}

function sendInput() {
    const dest = $("#destination").val()
    const input = {
        'author': $("#nickname").val(),
        'content': $("#input").val()
    };
    stompClient.send("/app/send/" + dest, {}, JSON.stringify(input));
}

function showMessage(message) {
    $("#chat-history").append("<tr>" +
        "<td>" + message.author + "</td>" +
        "<td>" + message.content + "</td>" +
        "<td>" + message.dateTime.substring(0, 10) + "</td>" +
        "<td>" + message.dateTime.substring(11, 19) + "</td>" +
        "</tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendInput();
    });
});

